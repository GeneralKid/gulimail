package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author zxl
 *
 * @date 2021-10-01 21:08:49
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
